﻿using CefSharp;
using CefSharp.Handler;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CefSharpDemo
{
    class MyResReplaceResourceRequestHandler : ResourceRequestHandler
    {
        MyResReplaceRequestHandler myResReplaceRequestHandler;
        public MyResReplaceResourceRequestHandler(MyResReplaceRequestHandler handler)
        {
            myResReplaceRequestHandler = handler;
        }

        protected override IResourceHandler GetResourceHandler(IWebBrowser chromiumWebBrowser, IBrowser browser, IFrame frame, IRequest request)
        {
            var replace = myResReplaceRequestHandler.GetReplaced(request.Url);
            if (replace == null)
            {
                return null;
            }
            else
            {
                if (replace is string)
                {
                    return ResourceHandler.FromFilePath(replace as string);
                }
                else if (replace is byte[])
                {
                    return ResourceHandler.FromByteArray(replace as byte[]);
                }
                else if (replace is Stream)
                {
                    return ResourceHandler.FromStream(replace as Stream);
                }
                return null;
            }
        }

        protected override CefReturnValue OnBeforeResourceLoad(IWebBrowser browserControl, IBrowser browser, IFrame frame, IRequest request, IRequestCallback callback)
        {
            myResReplaceRequestHandler.FireBeforeResourceLoad(request.Url);
            return base.OnBeforeResourceLoad(browserControl, browser, frame, request, callback);
        }
    }
}
