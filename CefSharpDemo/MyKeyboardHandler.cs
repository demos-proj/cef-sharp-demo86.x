﻿using CefSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CefSharpDemo
{
    class MyKeyboardHandler : IKeyboardHandler
    {
        public bool OnKeyEvent(IWebBrowser browserControl, IBrowser browser, KeyType type, int windowsKeyCode, int nativeKeyCode, CefEventFlags modifiers, bool isSystemKey)
        {
            return true;
        }

        public bool OnPreKeyEvent(IWebBrowser browserControl, IBrowser browser, KeyType type, int windowsKeyCode, int nativeKeyCode, CefEventFlags modifiers, bool isSystemKey, ref bool isKeyboardShortcut)
        {
            switch (windowsKeyCode)
            {
                case (int)Keys.F5:
                    browser.Reload();
                    return true;
                case (int)Keys.F12:
                    if(nativeKeyCode > 0) // 解决F12弹出两个开发者工具问题
                        browser.ShowDevTools();
                    return true;
            }
            return false;
        }
    }
}
