﻿using CefSharp;
using CefSharp.Handler;
using CefSharp.Internals;
using CefSharp.WinForms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Windows.Forms;

namespace CefSharpDemo
{
    public partial class Form1 : Form
    {
        CefSharp.WinForms.ChromiumWebBrowser _browser;
        readonly string HOME_URL = "https://www.baidu.com/";
        List<string> _resourceUrls = new List<string>();
        public Form1()
        {
            InitializeComponent();
            this.Load += Form1_Load;
            this.FormClosing += Form1_FormClosing;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            var handler = _browser.RequestHandler as MyResReplaceRequestHandler;
            handler.SaveReplaceRule();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            InitializeCefSettings();
            _browser = new ChromiumWebBrowser(string.Empty);
            _browser.TitleChanged += _browser_TitleChanged;
            _browser.LifeSpanHandler = new MyDisablePopupLifeSpanHandler();
            _browser.KeyboardHandler = new MyKeyboardHandler();
            _browser.MenuHandler = new MyDisableContextMenuHandler();
            _browser.Dock = DockStyle.Fill;
            this.panel1.Controls.Add(_browser);
            _browser.RequestHandler = CreateResourcesReplaceRequestHandler(); // 替换资源
            _browser.AddressChanged += _browser_AddressChanged; // 地址变化
            _browser.LoadingStateChanged += _browser_LoadingStateChanged; // 加载变化
            _browser.FrameLoadEnd += _browser_FrameLoadEnd;
            RegisterCSharpObjectToWeb(); // 注册js
            LoadUrl(HOME_URL);
        }

        /// <summary>
        /// 初始化cef设置
        /// </summary>
        private void InitializeCefSettings()
        {
            var settings = new CefSettings();
            // 设置缓存目录，下次账号自动登录
            var appDir = System.AppDomain.CurrentDomain.BaseDirectory;
            //settings.CachePath = System.IO.Path.Combine(appDir, "cef_cache");
            //settings.RemoteDebuggingPort = 8088;
            settings.Locale = "zh-CN"; // 右键菜单默认中文
            settings.AcceptLanguageList = "zh-CN"; // 网站默认中文
            settings.IgnoreCertificateErrors = true;
            settings.LogFile = System.IO.Path.Combine(appDir, "cef_log/" + Guid.NewGuid().ToString() + ".txt");
            //settings.CefCommandLineArgs.Add("ppapi-flash-path", System.AppDomain.CurrentDomain.BaseDirectory + "pepflashplayer.dll"); //指定flash的版本，不使用系统安装的flash版本
            //settings.CefCommandLineArgs.Add("ppapi-flash-version", "29.0.0.171");
            //settings.CefCommandLineArgs.Add("enable-media-stream", "1");
            //settings.CefCommandLineArgs.Add("no-proxy-server", "1");
            //settings.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.79 Safari/537.36";

            //settings.LogSeverity = LogSeverity.Verbose;
            if (!Cef.Initialize(settings))
                throw new Exception("Unable to Initialize Cef");
        }

        /// <summary>
        /// 替换网页中js、css、图片、html等资源
        /// </summary>
        /// <returns></returns>
        private IRequestHandler CreateResourcesReplaceRequestHandler()
        {
            var myResReplaceRequestHandler = new MyResReplaceRequestHandler();
            myResReplaceRequestHandler.BeforeResourceLoad += myResReplaceRequestHandler_BeforeResourceLoad;
            myResReplaceRequestHandler.ReadReplaceRule();
            var replace = myResReplaceRequestHandler.GetReplaced("https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png");
            if(replace == null)
            {
                // 百度图标 改为 搜狗图标
                myResReplaceRequestHandler.AddFileReplaceRule("https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png",
                    Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "logo_880x280_sogou.png"));
                //handler.AddFileReplaceRule("img/PCtm", @"D:\logo_sogou.png");
                //handler.AddBinaryReplaceRule("img/PCtm", new System.IO.MemoryStream(Properties.Resources.logo_880x280_sogou));
            }

            return myResReplaceRequestHandler;
        }

        /// <summary>
        /// 资源加载事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void myResReplaceRequestHandler_BeforeResourceLoad(object sender, MyResReplaceRequestHandler.BeforeResourceLoadEventArgs e)
        {
            Console.WriteLine("res:" + e.RequestUrl);
            var handler = sender as MyResReplaceRequestHandler;
            this.RunOnUIAsync(() =>
            {
                var count = this.dataGridView1.Rows.Count;
                var replaced = handler.GetReplaced(e.RequestUrl);
                var replacedText = replaced == null ? "未替换" : replaced.ToString();
                this.dataGridView1.Rows.Insert(0, count + 1, e.RequestUrl, replacedText, "替换");
            });
        }

        /// <summary>
        /// 网页加载状态变化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _browser_LoadingStateChanged(object sender, LoadingStateChangedEventArgs e)
        {
            if (e.IsLoading)
            {
                this.toolStripStatusLabelState.Text = "正在加载...";
            }
            else
            {
                this.toolStripStatusLabelState.Text = "加载完成";
            }
        }

        /// <summary>
        /// url地址变化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _browser_AddressChanged(object sender, AddressChangedEventArgs e)
        {
            RunOnUI(() => {
                this.toolStripTextBoxUrl.Text = e.Address;
            });
        }

        /// <summary>
        /// 网页标题变化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _browser_TitleChanged(object sender, TitleChangedEventArgs e)
        {
            RunOnUI(() => this.Text = e.Title);
        }

        private void _browser_FrameLoadEnd(object sender, FrameLoadEndEventArgs e)
        {
            _browser.GetBrowser().MainFrame.ExecuteJavaScriptAsync("CefSharp.BindObjectAsync('myWin');");
        }

        /// <summary>
        /// 将c#对象注册到网页中，从网页中可以直接访问该对象
        /// </summary>
        private void RegisterCSharpObjectToWeb()
        {
            _browser.JavascriptObjectRepository.ResolveObject += (s, eve) =>
            {
                var repo = eve.ObjectRepository;
                if (eve.ObjectName == "myWin")
                {
                    repo.Register("myWin", new MyObjectRegisterToWeb(), isAsync: true, options: BindingOptions.DefaultBinder);
                }
            };
        }

        private void LoadUrl(string url)
        {
            this.toolStripTextBoxUrl.Text = url;
            _browser.Load(url);
        }

        private void toolStripButtonBack_Click(object sender, EventArgs e)
        {
            if(_browser.GetBrowser().CanGoBack)
            {
                _browser.Back();
                //_browser.GetBrowser().GoBack();
            }
        }

        private void toolStripButtonForward_Click(object sender, EventArgs e)
        {
            if(_browser.GetBrowser().CanGoForward)
            {
                _browser.Forward();
                //_browser.GetBrowser().GoForward();
            }
        }

        private void toolStripButtonRefresh_Click(object sender, EventArgs e)
        {
            _browser.GetBrowser().Reload();
        }

        private void toolStripButtonHome_Click(object sender, EventArgs e)
        {
            _browser.Load(HOME_URL);
        }

        private void toolStripTextBoxUrl_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                LoadUrl(toolStripTextBoxUrl.Text);
                e.Handled = true;
            }
        }

        private void toolStripButtonOpenDevTools_Click(object sender, EventArgs e)
        {
            _browser.ShowDevTools();
        }

        private void toolStripButtonInvokeJs_Click(object sender, EventArgs e)
        {
            _browser.GetBrowser().MainFrame.ExecuteJavaScriptAsync("myWin.showMessage('hello winform');");
        }

        private void favorites_Click(object sender, EventArgs e)
        {
            var tsb = sender as ToolStripButton;
            if(tsb!=null)
            {
                switch (tsb.Text)
                {
                    case "腾讯视频":
                        LoadUrl("https://v.qq.com/");
                        break;
                    case "优酷视频":
                        LoadUrl("https://www.youku.com/");
                        break;
                    case "H5测试":
                        LoadUrl("http://html5test.com/");
                        break;
                    default:
                        break;
                }
            }
        }

        private void RunOnUI(Action action)
        {
            if (this.IsDisposed || !this.Created)
                return;

            try
            {
                this.Invoke(action);
            }
            catch (Exception ex)
            {
                
            }
        }

        private void RunOnUIAsync(Action action)
        {
            if (this.IsDisposed || !this.Created)
                return;

            this.BeginInvoke(new Action(()=> {
                try
                {
                    action();
                }
                catch (Exception ex)
                {
                    
                }
            }));
        }

        /// <summary>
        /// 替换资源
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0 || e.ColumnIndex < 0)
                return;
            if(e.ColumnIndex == 3)
            {
                var url = this.dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
                //MessageBox.Show(url);
                var handler = _browser.RequestHandler as MyResReplaceRequestHandler;
                var dialog = new OpenFileDialog();
                dialog.Multiselect = false;
                if(dialog.ShowDialog() == DialogResult.OK)
                {
                    handler.AddFileReplaceRule(url, dialog.FileName);
                    this.dataGridView1.Rows[e.RowIndex].Cells[2].Value = dialog.FileName;
                }
            }
        }

        // 查找资源url
        private int lastIndex = 0;
        private void buttonFindNextRes_Click(object sender, EventArgs e)
        {
            var find = this.textBoxResUrl.Text;
            for (int i = lastIndex + 1; i < this.dataGridView1.RowCount; i++)
            {
                var url = this.dataGridView1.Rows[i].Cells[1].Value.ToString();
                if (url.Contains(find))
                {
                    this.dataGridView1.CurrentCell = null;
                    this.dataGridView1.CurrentCell = this.dataGridView1.Rows[i].Cells[0];
                    this.dataGridView1.Rows[i].Selected = true;
                    lastIndex = i;
                    return;
                }
            }
            lastIndex = 0;
            MessageBox.Show("没有了");
        }

        private void toolStripButtonClearCookie_Click(object sender, EventArgs e)
        {
            Cef.GetGlobalCookieManager().DeleteCookies("", "");
        }
    }
}
