﻿using CefSharp;
using CefSharp.Handler;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CefSharpDemo
{
    class MyResReplaceRequestHandler : RequestHandler
    {
        List<string> urlPatterns = new List<string>();
        List<object> replaces = new List<object>();

        public event EventHandler<BeforeResourceLoadEventArgs> BeforeResourceLoad;

        public class BeforeResourceLoadEventArgs : EventArgs
        {
            public string RequestUrl;
        }

        string resReplaceDir;
        string resReplaceFile;

        public MyResReplaceRequestHandler()
        {
            resReplaceDir = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "resReplaceDir");
            if (!Directory.Exists(resReplaceDir))
                Directory.CreateDirectory(resReplaceDir);

            resReplaceFile = Path.Combine(resReplaceDir, "resReplaceFile.txt");
            if (!File.Exists(resReplaceFile))
                File.Create(resReplaceFile).Close();
        }

        protected override bool OnBeforeBrowse(IWebBrowser browserControl, IBrowser browser, IFrame frame, IRequest request, bool userGesture, bool isRedirect)
        {
            Console.WriteLine(request.Url);
            return base.OnBeforeBrowse(browserControl, browser, frame, request, userGesture, isRedirect);
        }

        public object GetReplaced(string urlPattern)
        {
            if (string.IsNullOrWhiteSpace(urlPattern))
                return null;
            for (int i = 0; i < urlPatterns.Count; i++)
            {
                var item = urlPatterns[i];
                if (Regex.IsMatch(urlPattern, item) || urlPattern == item || urlPattern.Contains(item))
                {
                    var replace = replaces[i];
                    return replace;
                }
            }
            return null;
        }

        public void AddFileReplaceRule(string urlPattern, string localFile)
        {
            AddReplaceRule(urlPattern, localFile);
        }

        public void AddBinaryReplaceRule(string urlPattern, Stream stream)
        {
            AddReplaceRule(urlPattern, stream);
        }

        public void AddBinaryReplaceRule(string urlPattern, byte[] bytes)
        {
            AddReplaceRule(urlPattern, bytes);
        }

        public void SaveReplaceRule()
        {
            var lines = new string[urlPatterns.Count];
            for (int i = 0; i < urlPatterns.Count; i++)
            {
                if(replaces[i] is string)
                    lines[i] = urlPatterns[i] + "," + replaces[i];
            }
            System.IO.File.WriteAllLines(resReplaceFile, lines);
        }

        public void ReadReplaceRule()
        {
            var lines = File.ReadAllLines(resReplaceFile);
            foreach (var item in lines)
            {
                var values = item.Split(',');
                AddReplaceRule(values[0], values[1]);
            }
        }

        private void AddReplaceRule(string urlPattern, object replace)
        {
            var foundIndex = urlPatterns.IndexOf(urlPattern);
            if(foundIndex == -1)
            {
                urlPatterns.Add(urlPattern);
                replaces.Add(replace);
            }
            else
            {
                replaces[foundIndex] = replace;
            }
        }

        protected override IResourceRequestHandler GetResourceRequestHandler(IWebBrowser chromiumWebBrowser, IBrowser browser, IFrame frame, IRequest request, bool isNavigation, bool isDownload, string requestInitiator, ref bool disableDefaultHandling)
        {
            return new MyResReplaceResourceRequestHandler(this);
        }

        public void FireBeforeResourceLoad(string url)
        {
            if (BeforeResourceLoad != null)
            {
                BeforeResourceLoad(this, new BeforeResourceLoadEventArgs()
                {
                    RequestUrl = url
                });
            }
        }
    }
}
